#!/bin/bash

# Check if input file is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 <input_file>"
    exit 1
fi

input_file="$1"
output_file="${input_file%.json}_converted.json"

# Check if jq is installed
if ! command -v jq &> /dev/null; then
    echo "jq is required but it's not installed. Aborting."
    exit 1
fi

# Convert Unix timestamp to ISO date with only the day
jq '.your_events_v2[] | .start_timestamp |= strftime("%Y-%m-%d", .start_timestamp)' "$input_file" > "$output_file"

echo "Conversion completed. Output written to $output_file"
