#!/bin/bash

# Check if input file is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 <input_file>"
    exit 1
fi

input_file="$1"
output_file="${input_file%.json}_processed.json"

# Check if jq is installed
if ! command -v jq &> /dev/null; then
    echo "jq is required but it's not installed. Aborting."
    exit 1
fi

# Remove attributes from JSON
jq '.events |= map(del(.end_timestamp, .place, .create_timestamp))' "$input_file" > "$output_file"

echo "Processing completed. Output written to $output_file"
