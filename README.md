# CHM Events repo

Add here any event that you want show in the CHM

## Concept: how does it work?

The `events` directory contains 3 subdirs:

* past
* next
* future

`past` and `future` directories can contain one or more `.json` file.
`next` directory MUST contain only one `.json` file.

Each `.json` file describes one **event**, including the speeches and the speakers involved.

The data structure of any event is described in the file `model.json`.

The CHM website pulls the `events repo` at fixed intervals and renders its content to the public.

## Usage

Get the repo on your pc

```
git clone git@gitlab.com:crypto-hub-malta/events.git
cd events
```

Copy `model.json` in the right directory and name it accordingly:

```
cp model.json future/2030-01-01_a.json
```

Edit `future/2030-01-01_a.json` in your favorite IDE. Save it.

Push the new content back to this repo and soon the new event will be displayed on the website.
