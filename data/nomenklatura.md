# How to store files in this folder

Any file in this folder must follow this nomenklatura:

`ISO-DATE_<what>.<extension>`

Ex.: 2023-08-22_teaser.jpg

**NO SPACES or SPECIAL CHARACTERS ALLOWED**

## ISO-DATE

iso-date has this format: YYYY-MM-DD (all numbers) like 2023-08-22 (22nd of August, 2023)

## `<what>`

`<what>` placeholder can be anything but there standards for some files:

* "teaser" if it's the teaser picture of an event
* "slides_X" if the files contains the slides for a speech. X starts from 1 and increments by one if there are more slides for the same day.

## `<extension>`

Most common:

* `<jpg>`
* `<pdf>`
* `<png>`

Note: teaser files should always be `.jpg`